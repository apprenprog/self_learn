# 再起関数でトリボナッチ数列のi番目を求める
# 0, 0, 1, 1, 2, 4, 7, 13, 24, 44, 81, 149

def tribo(sequence)  #トリボナッチ関数
    if sequence == 1
        return 0
    elsif sequence == 2
        return 0
    elsif sequence == 3
        return 1
    else
        return tribo(sequence - 1) + tribo(sequence - 2) + tribo(sequence - 3)
    end
end

i = gets.chomp.to_i  #入力

puts("#{i}番目:#{tribo(i)}")