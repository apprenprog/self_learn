#https://yukicoder.me/problems/746 より

input = []

while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!        
    input << line  
end

element_num = input[0].to_f #要素数
element = input[1].split(" ").map(&:to_f).sort! #要素
center_num = ((element_num / 2) - 1).to_f #中央値にあたる要素順番号

if element_num % 2 == 0 then
    puts ((element[center_num] + element[center_num + 1]) / 2.0).to_f.floor(1)
else
    puts element[center_num.ceil(0)].floor(1)
end