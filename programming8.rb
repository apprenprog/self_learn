#https://yukicoder.me/problems/783 より

get_num = []

str_divide = gets.chars.map(&:to_s) #入力

str_divide.each do |x|
    get_num << $&.to_i if /\d/ =~ x #正規表現の10進数に一致する時に$&にセット
end

if get_num.count == 0 then #入力に数値が含まれない時
    puts 0
else
    puts get_num.sum
end 