#https://yukicoder.me/problems/43  より

input = []
while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!     
    input.push(line)
end

step = input[0].split(" ").map(&:to_i)

step_num = step[1] / step[0]

if  step[1] % step[0] != 0  #最後の1歩
    step_num += 1
end

print step_num