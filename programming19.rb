#https://yukicoder.me/problems/no/29 より

input = []
while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!
    input << line
end


battle_num = input[0].to_i #バトル回数
get_item = [] #手に入れたすべてのアイテムのリスト
item_variety = [] #手に入れたアイテムの種類リスト
powerup = 0 #パワーアップ回数

for i in 1..battle_num
    get_item << input[i].split(" ")
    item_variety << input[i].split(" ")
end

get_item.flatten! #多次元を1次元のリストに
item_variety.flatten!.sort!.uniq! #配列をソートして重複を排除

#同種類パワーアップ
item_variety.each{|j|
    powerup += get_item.count(j) / 2 if get_item.count(j) >= 2
}

#4個パワーアップ
powerup += (get_item.length - (powerup * 2)) / 4

puts powerup