# https://yukicoder.me/problems/no/48 より

input = []
while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!       
    input << line.to_i
end

x = input[0] #ゴールX座標
y = input[1] #ゴールY座標
move = input[2] #移動可能距離
zahyou = [0, 0] #現在の座標
command = 0

if y > 0 then #Y方向へ先に移動する
    while zahyou[1] < y do
        zahyou[1] += move
        command += 1
    end
    
    if x != 0 then
        command += 1 #向きを変える
        
        while zahyou[0] < x.abs do
            zahyou[0] += move
            command += 1
        end
    end
elsif y < 0 then #X方向へ先に移動する
    
    command += 1 #向きを変える
    
    if x != 0
        while zahyou[0] < x.abs do
            zahyou[0] += move
            command += 1
        end
    end
    
    command += 1 #向きを変える
    
    while zahyou[1] < y.abs do
        zahyou[1] += move
        command += 1
    end
else #Y方向への移動が0の時
    if x != 0
        command += 1 #向きを変える
    
        while zahyou[0] < x.abs do
            zahyou[0] += move
            command += 1
        end
    end
end

puts command