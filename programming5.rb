#https://yukicoder.me/problems/560  より

input = gets #入力

num = input.split(" ").map(&:to_i)

for i in num[0]..num[1] do
    list = i.to_s.chars  #各桁ごとに分解
    puts i if i % 3 == 0 or list.include?("3") #3の倍数 or 3が付く数字
end