# https://yukicoder.me/problems/no/35 より

input = []
while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!       
    input << line
end

kukan_num = input[0].to_i
typing = []
typing_num = 0 #最終的にタイピングできる文字数
lost_num = 0 #最終的にタイピングできなかった文字数

for i in 1..kukan_num do
    typing << input[i].split(" ")
    m = typing[i-1][0].to_f
    typing_put = (12.0 * m).quo(1000).floor #1区間でタイピングできる文字数
    
    if typing_put >= typing[i-1][1].length then #1区間で全文字タイピングできる時
        typing_num += typing[i-1][1].length
    else
        typing_num += typing_put.to_i
        lost_num += (typing[i-1][1].length - typing_put.to_i)
    end
end

print(typing_num, " ", lost_num)
puts("")
