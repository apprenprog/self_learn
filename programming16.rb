#https://yukicoder.me/problems/no/51 より

input = [] 
while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!        
    input << line
end

w = input[0].to_f
d = input[1].to_f

while d >= 1 do
    m = w.quo(d**2).floor #作業量
    w -= m
    d -= 1
end

puts m