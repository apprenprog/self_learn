#https://yukicoder.me/problems/no/5 より

input = []
while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!        
    input << line
end

box_width = input[0].to_i #箱の幅
block_num = input[1].to_i #ブロックの個数
block = input[2].split(" ").map(&:to_i) #それぞれのブロックの個数
block.sort!

sum = 0
cnt = 0
i = 0

while sum <= box_width and (i + 1) <= block_num do
    sum +=  block[i]
    cnt += 1
    i += 1
end

if sum == box_width then #ブロックが箱にピッタリ収まる時
    puts cnt
elsif sum < box_width and i == block_num then #全てのブロックを使い切った時
    puts cnt
else
    puts cnt - 1
end
