#https://yukicoder.me/problems/134 より

input = gets.split(" ").map(&:to_i)
length = input[0] #ポッキーの長さ
bite = input[1] #1回にかじる長さ

bite_times = length / (bite * 2) #かじる回数

if (length % (bite * 2)) == 0
    bite_by_yu = (bite_times - 1) * bite
elsif
    bite_by_yu = bite_times * bite
end

puts bite_by_yu