#https://yukicoder.me/problems/no/56 より

require 'bigdecimal'

input = gets.split(" ").map(&:to_f)
price = input[0]
tax = input[1]

puts BigDecimal((BigDecimal(price.to_s) * (BigDecimal((tax * 0.01).to_s) + 1)).to_s).to_f.floor(0)