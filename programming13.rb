#https://yukicoder.me/problems/no/481 より

num = gets
num.delete!(" ")

(1..10).each{|i|
    if i == 1 and num.count("1") == 1 and num.include?("10") then #1を含まないとき
        puts i
    else #1以外を含まないとき
        puts i if num.include?(i.to_s) == false
    end
}