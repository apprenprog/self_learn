#https://yukicoder.me/problems/no/18 より

def usa_code(character, sequence) #うーさー復号メソッド
    code = {}
    hash_v = 1

    ("A".."Z").each{|hash_k|
        code.store(hash_k, hash_v.to_i)
        hash_v += 1
    }
    
    while sequence > 26 do #A→Zを2回以上またがないために
        sequence -= 26
    end
    
    if code[character] <= sequence then #復号でA→Zに跳ぶ時
        return code.key(26 - (sequence - code[character]))
    else
        return code.key(code[character] - sequence)
    end
end

input = gets.chomp.chars.map(&:to_s) #入力文
decryption = [] #復号文

for i in 0..(input.length - 1) do
    decryption << usa_code(input[i], (i + 1))
end

puts decryption.join("")
