# https://yukicoder.me/problems/492  より

def judge(wmonth, wday, wcnt) #月と日の和が等しいか判定
    if wmonth == (wday / 10) + (wday % 10) then
        wcnt += 1
    end
    return wcnt
end

cnt = 0

for month in 1..12 do
    if month == 2 then  #2月
        for day in 1..28 do
            cnt = judge(month, day, cnt)
        end
    elsif month == 4 or month == 6 or month == 9 or month == 11 then  #4月,6月,9月,11月
        for day in 1..30 do
            cnt = judge(month, day, cnt)
        end
    else  #それ以外の月
        for day in 1..31 do
            cnt = judge(month, day, cnt)
        end
    end
end

puts cnt
