#https://yukicoder.me/problems/no/564 より

input = [] 
while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!        
    input << line
end

nama = input[0].split(" ")
nama.map!(&:to_i)
input.delete_at(0)
input.map!(&:to_i)

rank = input.select{|x| x > nama[0]}.count + 1 #なま君の順位

if rank % 10 == 1 then
    puts("#{rank}st")
elsif rank % 10 == 2 then
    puts("#{rank}nd")
elsif rank % 10 == 3 then
    puts("#{rank}rd")
else
    puts("#{rank}th")
end
