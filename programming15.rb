#https://yukicoder.me/problems/35 より

input = []
while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理		
    line.chomp!
    input << line	
end	

#それぞれの文字列を1文字ずつに分解
str1 = input[0].chars.map(&:to_s)
str2 = input[1].chars.map(&:to_s)

#それぞれの文字列をソート
str1.sort!
str2.sort!

if str1.join("") == str2.join("") then #それぞれの文字列の構成が一致する時
    puts("YES")
else
    puts("NO")
end