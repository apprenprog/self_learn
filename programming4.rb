# 再起関数でトリボナッチ数列のi番目を求める(再起呼び出し1つのみ)
# 0, 0, 1, 1, 2, 4, 7, 13, 24, 44, 81, 149

def tribo1(sequence1)  #トリボナッチ関数
    return tribo2(0, 0, 1, sequence1)
end

def tribo2(a, b, c, sequence2)  #再起呼び出しトリボナッチ関数
    if sequence2 < 2 then
        return a
    else
        return tribo2(b, c, a + b + c, sequence2 - 1)
    end
end

i = gets.chomp.to_i

puts("#{i}番目:#{tribo1(i)}")