#https://yukicoder.me/problems/5  より

input = []

while line = gets     #入力のEOF(nil)に達するまで各行を読み込んでline変数に代入するwhile処理
    line.chomp!     
    input.push(line.to_i)
end
    
c1 = input[2]  #1円硬貨の枚数
c25 = input[1]  #25円硬貨の枚数
c100 = input[0]  #100円硬貨の枚数

c25 = c25 + c1 / 25
c100 = c100 + c25 / 4

c1 = c1 % 25
c25 = c25 % 4
c100 = c100 % 10

sum = c1 + c25 + c100
puts sum